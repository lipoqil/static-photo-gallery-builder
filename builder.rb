#!/usr/bin/env ruby

require 'yaml'
require 'active_support/all'
require 'mini_exiftool'
require 'erb'
require 'rmagick'
require 'ruby-progressbar'

using ProgressBar::Refinements::Enumerator

config = YAML.load_file 'config.yaml', symbolize_names: true
logger = Logger.new(STDOUT)
logger.formatter = proc do |severity, datetime, progname, msg|
  "#{datetime} #{severity.upcase} : #{msg}\n"
end
logger = ActiveSupport::TaggedLogging.new(logger)

galleries_list = config[:galleries].each_with_object({}) do |(gallery_name, gallery_config), galleries|
  next unless gallery_config[:enabled]

  galleries[gallery_name] ||= {name: gallery_config[:name], gallery: gallery_name, photos: [], keywords: {}}

  # Prepare gallery files and directories
  photos_full_output_path = "./output/#{gallery_name}/full"
  photos_thumbnails_output_path = "./output/#{gallery_name}/thumbnails"
  FileUtils.mkdir_p [photos_full_output_path, photos_thumbnails_output_path]
  FileUtils.cp_r 'assets/', "./output/#{gallery_name}/"

  puts "Processing #{gallery_name}"
  # Process photos
  photos_list = Dir.glob("./photos/#{gallery_name}/*.{jpg,jpeg}").reject do |photo_path|
    gallery_config[:skipped_photos].include?(File.basename(photo_path, '.*'))
  end
  photos_list.each.with_progressbar do |photo_filename, photos_progressbar|
    skip_image_file_operations = ((gallery_config[:skip_existing_files] || config[:skip_existing_files]) &&
      File.exist?("#{photos_full_output_path}/#{File.basename photo_filename}")
    )
    photos_progressbar.title = File.basename(photo_filename)[0..19]&.ljust(20, ' ')
    print "\e]9;4;1;#{ photos_progressbar.to_h['percentage'].to_i }\a"

    # Place the original
    FileUtils.cp(photo_filename, photos_full_output_path) unless skip_image_file_operations

    # Extract metadata
    photo = MiniExiftool.new photo_filename, coord_format: '%.6f'
    logger.tagged('metadata') do

      #<editor-fold desc="Fixing location">
      if (photo.gps_latitude.blank? || photo.gps_longitude.blank?) && gallery_config[:default_gps].present?
        photo.gps_latitude = gallery_config[:default_gps][:latitude]
        photo.gps_longitude = gallery_config[:default_gps][:longitude]
      end
      #</editor-fold>

      #<editor-fold desc="Hacking through time">
      logger.tagged('timezones') do
        if photo.country.blank? || photo.city.blank?
          logger.warn(
            "#{photo.filename} has Country '#{photo.country}' and City '#{photo.city}', so I am not touching that 🙌🏾"
          )
        else
          used_time_zone = begin Time.find_zone!(photo.city) rescue Time.find_zone!(photo.country) end
          Time.use_zone used_time_zone do
            time = photo.date_time_original
            photo.date_time_original = Time.zone.parse(
              "#{time.year}-#{time.month}-#{time.day} #{time.hour}:#{time.min}:#{time.sec}"
            )
            # photo.save!
            # FileUtils.touch("#{photo.filename}.timezone_fixed")
          end
        end
      end
      #</editor-fold>

      #<editor-fold desc="Building gallery's keywords">
      if photo.keywords.is_a? Array
        (photo.keywords - gallery_config[:skipped_keywords]).each do |keyword|
          galleries[gallery_name][:keywords][keyword] ||= {name: keyword, gallery: gallery_name, photos: []}
          galleries[gallery_name][:keywords][keyword][:photos] << photo
        end
      elsif photo.keywords.is_a?(String) && !gallery_config[:skipped_keywords].include?(photo.keywords)
        galleries[gallery_name][:keywords][photo.keywords.to_sym] ||= {name: photo.keywords.to_sym, gallery: gallery_name, photos: []}
        galleries[gallery_name][:keywords][photo.keywords.to_sym][:photos] << photo
      end
      #</editor-fold>
    end

    # Prepare thumbnails
    unless skip_image_file_operations
      magic_photo = Magick::ImageList.new photo_filename
      magic_photo.resize_to_fit(300, 200).write("#{photos_thumbnails_output_path}/#{File.basename photo_filename}")
    end

    galleries[gallery_name][:photos] << photo
  end
end

photo_list_template = ERB.new(File.read('./views/photo_list.html.erb'))
oembed_template = ERB.new(File.read('./views/oembed.json.erb'))

puts 'Generating pages'
galleries_list.each do |gallery_name, gallery_data|
  heading = gallery_data[:name]
  keywords = gallery_data[:keywords]
  keywords_page = false
  gallery_config = config[:galleries][gallery_data[:gallery]]
  galleries_output = photo_list_template.result(binding)
  File.write  "output/#{gallery_name}/index.html", galleries_output
  File.write  "output/#{gallery_name}/oembed.json", oembed_template.result(binding)

  keywords.each do |keyword_name, gallery_data|
    heading = "Keyword: #{keyword_name}"
    keywords_page = true
    galleries_output = photo_list_template.result(binding)
    File.write  "output/#{gallery_data[:gallery]}/keyword-#{gallery_data[:name]}.html", galleries_output
  end
end


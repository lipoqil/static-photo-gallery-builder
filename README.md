# Static photo gallery builder

## Tasks covered

Generates for each provided photo

- thumbnails
- separate gallery page for each keyword + one for all of them
- link to maps, from [EXIF](https://en.wikipedia.org/wiki/Exif) coordinates

## Dependencies

- The builder is written in Ruby, so you'll need [Ruby](https://www.ruby-lang.org) installed. The builder was written and tested on Ruby 3.2.0
- System dependencies are described in _Brewfile_, so if you have [Homebrew](https://brew.sh) installed, you can install the system dependecies by running `brew bundle`
- The builder is using some Ruby some gems, which can be installed via [Bundler](https://bundler.io) by running `bundle`

## Usage

1. Copy your photos to photos/<name of your gallery>
2. Copy config.yaml.example to config.yaml (only the first time)
3. In config.yaml there are placeholder data. Change those to reflect facts about your gallery.
4. Run `./builder.rb` (this may take units or tens of minutes)

## Example result

![Japan 2023 gallery screenshot](assets/japan-2023-screenshot.png)


